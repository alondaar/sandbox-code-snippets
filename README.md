# Sandbox Code Snippets

Here are code snippets to be added to the Sandbox System Builder for FoundryVTT

## Getting started

Most of these .js files are snippets that are meant to be added into the existing Sandbox script files.
Some of them may even already be included features!

Scripts Primarily Affected: a-entity.js
