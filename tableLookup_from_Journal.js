//COMPATIBILITY NOTES:
// Sandbox Version 12 // Foundry Version 9
// This code snippet goes into a-entity.js script file, somewhere around line#3400 (not inside of other functions or loops)

//ALONDAAR -- Table Lookup from expression: lookupJ(journalName;column;row;optionalDefault)
// JOURNAL NAME SHOULD BE UNIQUE FROM OTHER JOURNALS!
// ONLY ONE TABLE PER JOURNAL!
let getLookupJ = rollexp.match(/(?<=\blookupJ\b\().*?(?=\))/g);
if (getLookupJ != null) {
    for (let i = 0; i < getLookupJ.length; i++) {
        let tochange = "lookupJ(" + getLookupJ[i] + ")";

        let blocks = getLookupJ[i].split(";");
        let parseprop = await auxMeth.autoParser(blocks[0], actorattributes, citemattributes, true, false, number);
        let parseCol = await auxMeth.autoParser(blocks[1], actorattributes, citemattributes, false, false, number);
        let parseRow = await auxMeth.autoParser(blocks[2], actorattributes, citemattributes, false, false, number);
        let parseDefault = blocks[3];
        if (parseDefault == undefined)
            parseDefault = 0;
        else
            parseDefault = await auxMeth.autoParser(blocks[3], actorattributes, citemattributes, false, false, number);
        let replaceValue = parseDefault;

        var myJournal = game.journal.getName(parseprop);
        if (myJournal != null) {
            var myContent = myJournal.data.content;
            var doc = new DOMParser().parseFromString(myContent, "text/html");
            let myTable = doc.getElementsByTagName("TABLE")[0];
            if (myTable != null) {
                if (myTable.rows.length > 1) {
                    let foundcol = null;

                    for (var j = 0, col; col = myTable.rows[0].cells[j]; j++) {
                        let colchecker = col.innerText.match(parseCol);
                        if (colchecker != null) {
                            foundcol = j;
                            break;
                        }
                    }

                    if (foundcol != null)
                        for (var k = 1, row; row = myTable.rows[k]; k++) {
                            let rowchecker = row.cells[0].innerText.match(parseRow);
                            if (rowchecker != null) {
                                replaceValue = await auxMeth.autoParser(row.cells[foundcol].innerText, actorattributes, citemattributes, false, false, number);
                                break;
                            }
                        }
                }
                else
                    ui.notifications.error('lookupJ() -- Journal with the name "' + parseprop + '" needs a table with at least 2 rows');
            }
            else
                ui.notifications.error('lookupJ() -- Journal with the name "' + parseprop + '" does not contain a valid HTML table.');
        } else
            ui.notifications.error('lookupJ() -- Journal with the name "' + parseprop + '" does not exist.');

        rollexp = rollexp.replace(tochange, replaceValue);
        rollformula = rollformula.replace(tochange, replaceValue);
    }
}
